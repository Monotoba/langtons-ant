LangtonsAntsGame game;

void settings() {
  size(640, 480);
}

void setup() {
  game = new LangtonsAntsGame();
}

void draw() {
 background(255);
 //game.generate();
 game.run();
 game.show();
 delay(80);
}
