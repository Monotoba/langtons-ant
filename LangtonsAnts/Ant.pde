class Ant {

  float x, y;
  float w;
  int dir;

  color antColor = color(255, 0, 0);

  Ant(float x, float y, float w) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.dir = Direction.ANTDOWN;
  }
  
  float getX() {
   return this.x; 
  }
  
  void setX(float x) {
   this.x = x; 
  }
  
  float getY() {
   return this.y; 
  }
  
  void setY(float y) {
   this.y = y; 
  }
  
  int getDir() {
   return this.dir; 
  }

  void setDirection(int dir) {
    this.dir = dir;
  }

  class Direction {
    static final int ANTUP = 0;
    static final int ANTRIGHT = 1;
    static final int ANTDOWN = 2;
    static final int ANTLEFT = 3;
  }

  void move() { 
    switch(dir) {
    case Direction.ANTUP:
      this.y -= 1;
      break;
    case Direction.ANTRIGHT:
      this.x += 1;
      break;
    case Direction.ANTDOWN:
      this.y += 1;
      break;
    case Direction.ANTLEFT:
      this.x -= 1;
      break;
    }
    print("\nAnt(X,Y): ", this.x, this.y, "\n");
  }
  
  void show() {
    fill(antColor);
    stroke(0);
    rect(this.x*w, this.y*w, this.w, this.w);
  }
}
