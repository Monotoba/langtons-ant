class Cell {
  float x, y;
  float w;
  
  color occupiedColor = color(255, 0, 0);
  color liveColor = color(0, 0, 0);
  color emptyColor = color(255, 255, 255);
  
  // Processing only allows enums in top level classes :(
  //enum states {empty, occupied, live};
  int state;
  int previous_state;
  
  Cell(float x, float y, float w) {
   this.x = x;
   this.y = y;
   this.w = w;
   
   this.previous_state = this.state;
  }
  
  void savePreviousState() {
    this.previous_state = this.state; 
  }
  
  void newState(int state) {
   this.state = state; 
  }
  
  int getState() {
   return this.state; 
  }
  
  void show() {
   switch(this.state) {
     case States.empty:
       fill(emptyColor);
       break;
     case States.live:
       fill(liveColor);
       break;
   }
   stroke(0);
   rect(this.x, this.y, this.w, this.w);
  }
  
  // Processing only allows enums in
  // top level classes. So we use an
  // embedded class here.
  class States {
    static final int empty = 0;
    static final int live = 1;
  }
  
}
