
class LangtonsAntsGame {
  int w = 32;
  int columns, rows;

  Cell[][] board;
  Ant ant;

  LangtonsAntsGame() {
    columns = width/w;
    rows = height/w;
    board = new Cell[columns][rows];
    init();
  }

  void init() {
    print("init()");
    for (int i = 0; i < columns; i++) {
      for (int j = 0; j < rows; j++) {
        board[i][j] = new Cell(i*w, j*w, w);
      }
    }

    float pos_x = floor(random(columns-1));
    float pos_y = floor(random(rows-1));
    this.ant = new Ant(pos_x, pos_y, w); 
    ant.setDirection(1);
    board[(columns/2)%columns][(rows/2)%rows].newState(Cell.States.empty);
    print("\nAnt X: ", pos_x);
    print("\nAnt y: ", pos_y, "\n");
  }

  void run() {
    print("run()");
    int pos_x = (int) ant.getX(); // ant returns pixel location and must be converted to columns/rows
    int pos_y = (int) ant.getY();
    
    // Apply rules
    // 1. At a white (empty) square, turn 90° right, flip the color of the square (to live), move forward one unit
    // 2. At a black (live) square, turn 90° left, flip the color of the square (to empty), move forward one unit
    // The cells on our board can have three states, empty, occupied, and live. Live and occupied
    // only differ in that in an occupied cell the ant is present. 
    int curDir = ant.getDir();
    int newDir = curDir;
    if (board[pos_x][pos_y].getState() == Cell.States.empty) {
      newDir = (curDir + 1) % 4;
      board[pos_x][pos_y].newState(Cell.States.live);
    } else if (board[pos_x][pos_y].getState() == Cell.States.live) {
      if ((curDir-1) < 0){
        newDir = 3;
      } else {
        newDir = curDir-1;  
      }
      board[pos_x][pos_y].newState(Cell.States.empty);
    } else
    ant.setDirection(newDir); // turn
    moveAnt();
  }
  
  void moveAnt() { 
    print("antMove()");
    float x = ant.getX();
    float y = ant.getY();
    print("Before Move x: ", x, "y: ", y);
    switch(ant.getDir()) {
    case Ant.Direction.ANTUP:
      y -= 1;
      if(y < 0) x = rows-1;
      break;
    case Ant.Direction.ANTRIGHT:
       x += 1;
      if(x > columns-1) x = 0;
      break;
    case Ant.Direction.ANTDOWN:
      y += 1;
      if(y > rows-1) y = 0;
      break;
    case Ant.Direction.ANTLEFT:
      x -= 1;
      if(x < 0) x = columns-1;
      break;
    }
    ant.setX(x);
    ant.setY(y);
    print("After x: ", x, "y: ", y);
    print("\nAnt(X,Y): ", x, y, "\n");
  }

  void show() {
    for (int i = 0; i < columns; i++) {
      for (int j = 0; j < rows; j++) {
        board[i][j].show();
      }
    }
    ant.show();
  }
}
